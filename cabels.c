#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char **argv) {
    int spans = 0;
    int dist = 0;
    int cabel_len = 0;

    if (argc == 3) {
        spans = atoi(argv[1]);
        dist = atoi(argv[2]);
        cabel_len = dist * ((spans * (spans + 1)) / 2);
        printf("%d\n", cabel_len);
    } else {
        write(2, argv[0], strlen(argv[0]));
        write(2, ": wrong number of arguments, expected 2\n", 40);
        exit(1);
    }
    return 0;
}
